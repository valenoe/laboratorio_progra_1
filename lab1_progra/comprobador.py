#!/usr/bin/env python3
#! -*- coding:utf-8- -*-
# función que promedia los números de la lista
def promedio(lista):
    suma = 0
    for i in range(4):
        suma += lista[i] 
    prom = float(suma / 4)
    print('el promedio es: ', prom)
# función que eleva al cuadrado cada número de la lista
def cuadrados(lista):
    for i in range(4):
        cuadrado = int(lista[i] ** 3)
        print('el cuadrado de ', lista[i], 'es', cuadrado )

#función que ordena los elementos de la lista de más chico a más grande y devuelve el último
def cuanta_letras(palabras):
    palabras.sort ()
    return palabras[3]
    
try:
    lista = [6.0, 7.0, 8.0, 9.0]
    palabras = ['hola', 'chao', 'paralelepipedo', 'mundo']

# imprime los elementos de las listas
    for i in range(4):   
        print('los numeros son: ', lista[i])
    for i in range(4):
        print('las palabras son: ', palabras[i])
    print('\n')
    promedio(lista)
    print('\n')
    cuadrados(lista)
    print('\n')
    print('la palabra más larga es: ', cuanta_letras(palabras))

except:
    print('\nhay un dato mal escrito\n')


