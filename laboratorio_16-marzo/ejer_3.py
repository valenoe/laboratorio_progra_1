#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

def _notas():
    '''
    crea las notas de forma aleatoria
    '''
    notas = []
    #for i in range(5):
    cont = 0
    while True: 
        x = round(random.uniform(1 , 7), 1)
        notas.append(x)
        cont += 1
        if cont == 5:
            break
    return notas

def estudiantes_notas(base):
    '''
    le asigna las notas a los 31 estudientes, 
    también añade los datos juntos (estudiantes, notas) a una lista
    '''
    for i in range(31):
        notas = _notas()
        base.append([i+1, notas])
    imprimir(base)


def imprimir(base):
    '''
    imprime
    '''
    print("\n\n  NOTAS \n\n")
    for i in range(31):
        for j in range(2):        
            print(base[i][j],end=' ')
        print()

def promedio(base):
    '''
    calcula el promedio de cada estudiante
    base = [[estudiantes, notas[]], [estudiantes, notas[]]...
    aquí solo lee las notas, este es el segundo elemento(lista) de cada lista 
    '''
    print("\n")
    for i in range(31):
        if base[i][1]:
            promedio = sum(base[i][1]) / 5
            if promedio < 4:
                print("el estudiante {0} esta reprobado con un {1}".format(i, promedio))
            else:
                print("el estudiante {0} esta aprobado con un {1}".format(i, promedio))


# Main
if __name__ == "__main__":
    print('')
    progra_1 = []
    estudiantes_notas(progra_1)
    promedio(progra_1)