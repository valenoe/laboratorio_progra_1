#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
'''
Realice un programa que lea una tupla que usted defina de n elementos. A partir de esa tupla rellene
una lista (por defecto debe estar vacı́a) de tal forma que el primer elemento pase a ser el segundo, el
segundo pase a ser el tercero, el último pase a ser el primero, y ası́ sucesivamente hasta recorrer toda
la tupla.
Recuerde que:
tupla = ()
lista = []
'''
def recorre(tupla):
    '''
    recoge una tupla definida y traspasa sus datos corridos en 1 espacio
    a la lista  
    '''
    x = len(tupla)
    
    lista = []
    lista.append(tupla[x-1])
    
    for i in range(x-1):
        lista.append(tupla[i])
    print("tupla: ", tupla)
    print("lista resultante: ", lista)



# Main
if __name__ == "__main__":
    
    tupla = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    recorre(tupla)