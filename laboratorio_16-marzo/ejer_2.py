#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Se tiene un “matriz” de      15 filas     y    12 columnas     . Realice un programa en Python que permita leer la
matriz, calcule y presente los resultados siguientes:
   - El menor elemento de la matriz
   - La suma de los elementos de las cinco primeras columnas de cada fila de la matriz.
   - Muestre el total de elementos negativos en las columnas de la quinta a la nueve.
Para realizar este ejercicio NO puede utilizar las funciones min, sum ni ninguna otra que le sean propias
a las listas, se pide utilizar la lógica para resolver el problema.
Para rellenar la matriz puede utilizar la siguiente sintaxis de Python:
      import random
      random . randint ( -10 ,10)
Esto permitirá crear número aleatorios entre -10 y 10.
Por último siempre considere el concepto de Python listas sobre listas es similar a trabajar con matrices.

'''
import random

def matrix(base):
    '''
    se crea la matriz con números aleatorios
    del -10 al 10 y se imprime
    '''
    for i in range(15):
        base.append([])
        for j in range(12):
            base[i].append(random.randint(-10,10))
            print(base[i][j],end=' ')
        print()

def menor_elemento(base):
    '''
    función que devuelve el valor menor de tod la "matriz"
    para eso la recorre comprando cada valor con "menor"
    menor originalmente es None, luefgo se cambia por el primer valor,
    este es el que se compara
    '''
    menor = None
    for i in range(15):
        for j in range(12):
            if menor == None:
                menor = base[i][j]
            else:
                if base[i][j] < menor:
                    menor = base[i][j]
    print("\nel elemento menor es: ", menor)



def suma_primeras_5_columnas_cada_fila(base):
    '''
    recorre solo los primeros 5 elementos de cada fila
    y los pasa a una función que devuelve la suma de estos
    '''
    print("\n\nSUMAS\n\n")
    for i in range(15):
        a_sumar = []
        for j in range(5):
            a_sumar.append(base[i][j])
        s = suma(a_sumar)
        
        print("la suma de de los 5 primeros elementos de la fila {0} es {1}".format(i+1, s))

def suma(a_sumar):
    suma = 0
    for i in range(5):
        suma = suma + a_sumar[i]
    return(suma)
    
def elemnetos_negativos(base):
    ''' 
    el for solo recorre las colmunas 5, 6, 7, 8 y 9 mostrandolas
    tambien compara cada elemento si es que es menor a 0
    e imprime la cantidad de elementos negativos 
    '''

    contador = 0
    print("\nlas columnas 5, 6, 7, 8 y 9 son las siguientes: \n")
    for i in range(15):
        for j in range(4,9):
            if base[i][j] < 0:
                contador += 1
            print(base[i][j],end=' ')
        print()
    print("hay {0} elementos negativos entre estas columnas".format(contador))
            



# Main
if __name__ == "__main__":
    base = []
    matrix(base)
    menor_elemento(base)
    suma_primeras_5_columnas_cada_fila(base)
    elemnetos_negativos(base)