#!/usr/bin/env python3
#! -*- coding:utf-8- -*-

import csv

def cargar():
    '''
    se abre y procesa el archivo 
    como datos diferentes por separación
    '''
    archivo = open('covid_19_data.csv', 'r')
    linea = csv.reader(archivo, delimiter=',')

    '''
    donde la primera linea son los títulos por los que se separan los datos, esta no se cuenta
    '''
    next(linea, None)

    '''
    Diccionarios para cada tarea:
    1) Contar y mostrar los países que hasta el momento cuentan con a lo menos un contagiado.
    2) Listar de cada País el total de infectados confirmados.
    3) Suma cuantos recuperados y cuantos muertos por país existen hasta el momento de generar este
    archivo.
    '''
    # 1 y 2
    num_confirmados = {}
    # 3
    num_muertos_y_recuperados = {}

    '''
    ciclo que rellena los diccionarios con los paices y los datos 
    corespondientes
    '''
    for dato in linea:
        pais = dato[3]
        confirmados = float(dato[5])
        muertos = float(dato[6])
        recuperados = float(dato[7])
        rellenar(pais, confirmados, num_confirmados)
        rellenar_y_sumar(pais, muertos, recuperados, num_muertos_y_recuperados)

    print("\n\n---PAICES CON AL MENOS 1 INFECTADO---\n\n")
    for pais in num_confirmados:
        if num_confirmados[pais] >= 1:
            print(pais)

    print("\n\n\n\n---TOTAL DE INFECTADOS POR PAÍS---\n\n")
    for pais in num_confirmados:
        print(pais, ":", str(num_confirmados[pais]))

    print("\n\n\n\n---TOTAL DE RECUPERADOS Y MUERTOS POR PAÍS---\n\n")
    for pais in num_muertos_y_recuperados:
        print(pais, ":", str(num_muertos_y_recuperados[pais]))


def rellenar(pais, dato, dic):
    if pais in dic:
        dic[pais] += dato
    else:
        dic[pais] = dato

def rellenar_y_sumar(pais, dato1, dato2, dic):
    if pais in dic:
        dic[pais] += dato1 + dato2
    else:
        dic[pais] = dato1 + dato2


if __name__ == "__main__":
    cargar()