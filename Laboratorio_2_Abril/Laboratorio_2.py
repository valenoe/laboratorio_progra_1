#!/usr/bin/env python3
# -*- coding:utf-8 -*-



import csv

def abrir_archivo():
    '''
    abre al archivo en modo lectura --> r
    y lo devuelve en formato lista 
    '''
    archivo = open('top50.csv', 'r')
    top50 = csv.reader(archivo, delimiter=',')

    # Salta la primera línea ya que son los títulos
    next(top50,None)

    return top50

def seccion_a(artistas):
    print("\n\n\n Ítem a \n\n\n")

    '''
    muestra cuantos artistas hay 
    y cual tiene más canciones
    '''

    print("hay {0} artistas".format(len(artistas)))

    canciones = 0
    for key, values in artistas.items():
        if len(values) > canciones:
            canciones = len(values)
            artista = key

    print("el artista con más canciones es {0} con {1} canciones".format(artista, canciones))


def busca(ranking, llave):

    '''
    Guarda los datos correspondientes al valor de cada llave en una lista 
    para usarse en otras funciones
    '''
    lista = []
    for values in ranking.values():
        for i in values:
            if i == llave:
                lista.append(values[llave]) 
    return lista


def seccion_b(ranking):
    print("\n\n\n Ítem b \n\n\n")

    '''
    Esta función calcula la mediana del ruido
    para ello crea una lisa l_ruido para asignarle
    los valores en cada canción
    '''
    l_ruido = busca(ranking, llave = "ruido")
    
    # se ordena la lista de menor a mayor
    l_ruido.sort()

    # Sabiendo que la lista tiene una cantidad de elementos par
    # se buscan 2 datos centrales
    x = int(len(l_ruido) / 2)
    mediana = []
    for i in range(x):
        # como el rango solo es la mitad de la lista
        # se busca el ultimo elemnto que esté dentro de este rango y otro inmediatamente siguiente
        # estos se agregan a otra lista creada anteriormente
        if i == x-1:
            mediana.append(l_ruido[i])
            mediana.append(l_ruido[i+1])
    
    # siendo 2 elemntos centrales
    # la maedia será su promedio
    mediana = int(sum(mediana) / 2)
    print("La mediana es = ", mediana)
    
    # busca cuantas canciones tegan el dato entregado en mediana
    # como ruido
    total = 0
    for i in l_ruido:
        if i == mediana:
            total += 1
    print("hay {0} canciones que calzan en la mediana".format(total))

def imprimir_seccion_c(ranking, lista):

    for key , values in ranking.items():
        for i in lista:
            if values["danzabilidad"] == i:
                lugar = key
                cancion = values["cancion"]
                print("  posición {0} : {1}".format(lugar, cancion) )

def seccion_c(ranking):
    
    '''
    Esta función busca las 3 canciones
    más bailables y las 3 más lentas
    '''

    danza = busca(ranking, llave = "danzabilidad")

    # ordena los datos
    danza.sort()

    # limita el rango para copiar en cada lista
    x = len(danza) - 3
    y = len(danza) - 47
    danza_1 = danza[x:]
    lento = danza[:y]  
    # coloca los mayores primero
    danza_1.reverse()
    print("\n\n\n Ítem c \n\n\n")
    print("Las canciones más vailables")
    imprimir_seccion_c(ranking, danza_1)
    print("\nlas canciones más lentas")
    imprimir_seccion_c(ranking, lento)


    
def ranking_ruido(nombre, cancion, genero, ruido, danza, energy, polularidad):
    
    '''
    Esta funcion crea y devuelve un diccionario
    con los datos correspondientes
    para introducirse en otro diccionario
    '''

    dato = {}
    dato["cancion"] = cancion
    dato["artista"] = nombre
    dato["genero"] = genero
    dato["ruido"] = ruido
    dato["danzabilidad"] = danza
    dato["energy"] = energy
    dato["polularidad"] = polularidad

    return dato

def datos_(top50):

    '''
    datos_ crea los diccionarios para responder cada item
    '''
    
    artistas = {}
    ranking = {}

    '''
    ya que cada linea de datos está distribuida en listas
    se asigna cada dato con su indice al nimbre que corresponde
    estos se ocuparan más adelante
    '''
    for dato in top50:
        nombre_a = dato[2]
        cancion = dato[1]
        posicion = int(dato[0])
        genero = dato[3]
        ruido = int(dato[7])
        Danceability = int(dato[6])
        Energy = int(dato[5])
        polularidad = int(dato[13])

        # para  diccionario artistas
        # si el artista ya se encuentra en el diciionario
        # se agrega la cancion a su lista de canciones
        if nombre_a in artistas:
            artistas[nombre_a].append(cancion)

        
        # si no está, se crea la lista del artista
        # y se agrega la canción
        else:
            artistas[nombre_a] = []
            artistas[nombre_a].append(cancion)

        
        # para diccionario ranking
        ranking[posicion] = ranking_ruido(
                                        nombre_a, cancion,
                                        genero, ruido, Danceability, 
                                        Energy, polularidad
                                        )
    
    seccion_a(artistas)  
    seccion_b(ranking) 
    seccion_c(ranking) 
 


if __name__ == "__main__":
    datos = abrir_archivo()
    datos_(datos)

    

