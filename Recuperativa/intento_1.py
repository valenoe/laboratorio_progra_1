#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import json

def abrir():

    # función que abre el archivo
     
    with open('iris.json') as file:
        datos = json.load(file)

    
    return datos

def nombre(datos):
    '''
        - Esta función crea una lista con 
          las diferentes especies verificando 
          que no se repita ninguna.
        - luego imprime.
    '''

    print("----Nombres----\n")
    lista = []
    for dato in datos:
        if dato["species"] not in lista:
            lista.append(dato["species"])
    for i in lista:
        print("especie :", i)

def datos_utiles(datos):
    '''
        Datos de ancho y alto de los petalos de cada especie se guardan da la 
        siguente manera: 

            d_especie = [(alto, ancho), (alto, ancho) ... ]

        Datos de largo de los sépalos de cada especie:

            Alto_sepalo_especie = []

        Datos totales de cada especie:

            datos = [
                        {
                            "sepalLength": 7.0,
                            "sepalWidth": 3.2,
                            "petalLength": 4.7,
                            "petalWidth": 1.4,
                            "species": "versicolor"
                        },
                        ...
                    ]


        Esta función se encarga de insertar los datos necesarios
        para cada pregunta en listas a ocupar en distintas funciones    
    '''
    # PÉTALOS
    # petalWidth = ancho
    # petalLength = alto
    d_setosa = []
    d_versicolor = []
    d_virginica = []

    # SÉPALOS
    Alto_sepalo_setosa = []
    Alto_sepalo_versicolor = []
    Alto_sepalo_virginica = []

    #JSON
    d_totales_setosa = []
    d_totales_versicolor = []
    d_totales_virginica = []

    for dato in datos:
        if dato["species"] == "setosa":
            d_setosa.append((dato["petalLength"], dato["petalWidth"]))
            Alto_sepalo_setosa.append(dato["sepalLength"])
            d_totales_setosa.append(dato)
        elif dato["species"] == "versicolor":
            d_versicolor.append((dato["petalLength"], dato["petalWidth"]))
            Alto_sepalo_versicolor.append(dato["sepalLength"])
            d_totales_versicolor.append(dato)
        else:
            d_virginica.append((dato["petalLength"], dato["petalWidth"]))
            Alto_sepalo_virginica.append(dato["sepalLength"])
            d_totales_virginica.append(dato)

    print("\n----Promedios----\n")

    al_set = promedio(d_setosa, indice = 0,  key = "setosa", dato = "alto")
    an_set = promedio(d_setosa, indice = 1,  key = "setosa", dato = "ancho")
    al_ver = promedio(d_versicolor, indice = 0,  key = "versicolor",  dato = "alto")
    an_ver = promedio(d_versicolor, indice = 1,  key = "versicolor", dato = "ancho")
    al_vir = promedio(d_virginica, indice = 0,  key = "virginica", dato = "alto")
    an_vir = promedio(d_virginica, indice = 1,  key = "virginica", dato = "ancho")
    print('\n')
    comparar(al_set,  an_set, al_ver, an_ver, al_vir, an_vir)
    print("\n----Registros----")
    registros(al_set, an_set, d_setosa, key = "setosa")
    registros(al_ver, an_ver, d_versicolor, key = "versicolor")
    registros(al_vir, an_vir, d_virginica, key = "virginica")
    print("\n----Sépalos----\n ")
    comparar_2(Alto_sepalo_setosa, Alto_sepalo_versicolor, Alto_sepalo_virginica )

    diccionario(d_totales_setosa, ruta = "setosa.json")
    diccionario(d_totales_versicolor, ruta = "versicolor.json")
    diccionario(d_totales_virginica, ruta = "virginica.json")

    

def promedio(lista, indice, key, dato):
    '''
    esta función calcula e imprime el promedio de lo
    que se llame en 'dato' --> ancho o alto
    para cada 'key' --> especie
    y devuelve su valor a una variable

    lista es un conjunto de tuplas
    indice = 0 --> alto
    indice = 1 --> ancho 
    de cada petalo de las distintas especies

    convierte los datos del indice 0 y 1 de cada tupla en 2 listas 
    estas las suma y promedia
    '''
    
    datos = []
    for i in lista:
        datos.append(i[indice])

    prom_datos = sum(datos) / len(lista)
    print("El promedio del {2} de los petalos de {0} es : {1:.2f}".format(key, prom_datos, dato))

    return prom_datos

def comparar(al_set, an_set, al_ver, an_ver, al_vir, an_vir):

    '''
    - Recibe los promedios de ancho(an) y alto(al) de cada especie.
    - Los agrega a listas de ancho y alto.
    - Las ordena de menor a mayor.
    - Considerando que son 3 especies, el elemento mayor será el de indice 2.
    - Compara este ultimo elemento con los recibido al primcipio
      para agregar una especie .
    - Luego se imprime todo, con un límite de 2 decimales para el promedio.
    '''

    al = [al_set, al_ver, al_vir]
    an = [an_set, an_ver, an_vir]
    al.sort
    an.sort
    if al[2] == al_set:
        x = "setosa"
    elif al[2] == al_ver:
        x = "versicolor"
    else:
        x = "virginica"

    if an[2] == an_set:
        y = "setosa"
    elif an[2] == an_ver:
        y = "versicolor"
    else:
        y = "virginica"

    print("El promedio de petalos más altos es {0:.2f} de la especie: {1}".format(al[2], x))
    print("El promedio de petalos más anchoos es {0:.2f} de la especie: {1}".format(an[2], y))

def registros(prom_al, prom_an, tupla, key):
    '''
        En un rango de -3 y +3 con respecto al promedio
        Se calculan los petalos que calzan dentro de este rango
        para alto y ancho de cada especie(key)

    '''

    min_al = prom_al - 3
    max_al = prom_al + 3

    min_an = prom_an - 3
    max_an = prom_an + 3

    contador_alto = 0
    for dato in tupla:
        if min_al < dato[0] < max_al:
            contador_alto +=1
    contador_ancho = 0
    for dato in tupla:
        if min_an < dato[1] < max_an:
            contador_ancho +=1


    print("\n{0}:\n promedio alto = {1:.2f}\n rango = {2:.2f} y {3:.2f}".format(key, prom_al, min_al, max_al))
    print(" Hay {0} registros que entran en este rango".format(contador_alto))
    print("\n promedio ancho = {0:.2f}\n rango = {1:.2f} y {2:.2f}".format(prom_an, min_an, max_an))
    print(" Hay {0} registros que entran en este rango".format(contador_ancho))
        

def comparar_2(setosa, versicolor, virginica):
    '''
        Busca el dato mayor en las litas
        de datos de altura de los sépalos de cada especie
    
        compara e imprime la correspondiente a la mayor altura
        junto con su especie
    '''

    setosa_m = max(setosa)
    versicolor_m = max(versicolor)
    virginica_m = max(virginica)


    if setosa_m > versicolor_m:
        if setosa_m > virginica_m:
            print("Setosa tiene los sépalos más largos, el mayor mide: ", setosa_m)

    if versicolor_m > setosa_m:
        if versicolor_m > virginica_m:
            print("Versicolor tiene los sépalos más largos, el mayor mide: ", versicolor_m)

    if virginica_m > setosa_m:
        if virginica_m > versicolor_m:
            print("Virginica tiene los sépalos más largos, el mayor mide: ", virginica_m)

    
def diccionario(datos, ruta):

    '''
    Recibe todos los datos de una especie 
    especificada en la ruta:
        
        ej: "virginica.json"

    y escribe un archivo json con esosdatos

    '''

    
    with open(ruta, "w") as archivos:
        json.dump(datos, archivos, indent=4)




if __name__ == "__main__":
    iris = abrir()
    nombre(iris)
    datos_utiles(iris)
